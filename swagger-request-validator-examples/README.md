# Swagger Request Validator - Examples #

Example usages of the Swagger Request Validator and its various integrations and adapters.

Examples in the main source tree (`src/main/java`) demonstrate usage of the standalone validator and adapters
that might be used in production code.

Examples in the test source tree (`src/test/java`) demonstrate usage within
unit tests, or integrations with mocking libraries ([Pact](https://github.com/DiUS/pact-jvm),
[Wiremock](http://wiremock.org/) etc.).